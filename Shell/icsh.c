#include <fcntl.h>
#include <unistd.h>
#include <signal.h>
#include <termios.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
//handle strings that are seperated by tabs, spaces, new lines etc. (to strip).
#define TOK_DELIM " \t\r\n\a"
#define SPACE " "
#define BUFFERSIZE 64

#define MAX_ARG 10
#define BASETEN 10

#define FOREGROUND 1
#define BACKGROUND 2

#define PAUSE 8
#define RUNNING 9

/* A job is a pipeline of processes.  */
typedef struct job{
  struct job *next;           /* next active job */
  char *command;              /* command line, used for messages */
  // process *first_process;     /* list of processes in this job */
  pid_t pgid;                 /* process group ID */
  char notified;              /* true if user told about stopped job */
  struct termios tmodes;      /* saved terminal modes */
  int stdin, stdout, stderr;  /* standard i/o channels */
  int tag;
  int completed;
  int stopped;
  char* name;

  int mode; /* foreground background */
  int runningStatus; /*running, pausing */
} job;

/* The active jobs are linked into a list.  This is its head.   */
job *first_job = NULL;
int jobcount = 0;

pid_t shell_pgid;
struct termios shell_tmodes;
int shell_terminal;
int shell_is_interactive;
volatile int shell_returncode; //for echo $ previous return code
volatile int loopstatus; // decalre it volatile so the value is sync within the threading process.
volatile sig_atomic_t status = 0;

void init_shell();
job *find_job(pid_t pgid);
// int job_is_stopped(job *j);
// int job_is_completed(job *j);
void launch_process(char* argv[],int infile, int outfile, int errfile, int mode);
void launch_job(job *j, int foreground);
void put_job_in_foreground(job *j, int cont);
void put_job_in_background(job *j, int cont);
void wait_for_job(job *j);
// void do_job_notification(void);

void signal_handler(int signum, siginfo_t* siginfo, void* context);
void child_signal_handler(int signum, siginfo_t* siginfo, void* context);
job *find_job_by_id(int id);
void icsh_execute(int mode, char** arg, int input, int error, int output, char* commandName);
int checkBuiltint(char *[]);
int icsh_bg(char *[]);
int icsh_echo(char *[]);
int icsh_exit(char *[]);
int icsh_fg(char *[]);
int icsh_jobs(char *[]);
int icsh_help(char *[]);
int icsh_cleanjob(job *j);
void ChildHandler (int sig, siginfo_t *sip, void *notused);

char* builtin_str[] = {
  "help",
  "exit",
  "echo",
  "fg",
  "bg",
  "jobs"
};

int (*builtin_func[])(char *[]) = {
  &icsh_help,
  &icsh_exit,
  &icsh_echo,
  &icsh_fg,
  &icsh_bg,
  &icsh_jobs
};


void signal_handler(int signum, siginfo_t* siginfo, void* context)
{
  sigset(SIGINT, SIG_DFL);
 switch(signum) {
 case SIGINT:
   status = 1;
   break;
 }
}

void child_signal_handler(int signum, siginfo_t* siginfo, void* context)
{
  // sigset(SIGINT, SIG_DFL);
 int status;
 pid_t pid;
 fflush(stdout);
 status = 0;
 pid = waitpid(siginfo->si_pid, &status, WNOHANG | WUNTRACED);
 if(pid == siginfo->si_pid) {
   if (WIFEXITED(status)) {
     // printf("child died\n");
     // printf("Detecting signal ....!\n");
     job* job = find_job(pid);
     // printf("dealing this job id : %d\n", job->tag);
     if (job->mode == BACKGROUND){
       printf("\n[%d]  - %d done %15s \n", job->tag, job->pgid, job->name);
       fflush(stdout);
     }
     shell_returncode = WEXITSTATUS(status);
     icsh_cleanjob(job);
   } else if (WIFSIGNALED(status)){
     printf("child signaled\n");
     shell_returncode = WTERMSIG(status);
   } else if (WIFSTOPPED(status)){
     job* job = find_job(pid);
     if(job!=NULL){
       if(job->runningStatus == PAUSE){
         wait_for_job(job);
         icsh_cleanjob(job);
       }
       else{
         job->runningStatus = PAUSE;
      }
      printf("[%d]  + %d suspended  %s\n", job->tag, pid, job->name);
      }
     fflush(stdout);
     shell_returncode = WSTOPSIG(status);
   } else {
     shell_returncode = siginfo->si_status;
     /* something else */
   }
 }
 else {
   // printf("Uninteresting: %d\n", pid);
   // fflush(stdout);
 }
}

/* A process is a single process.  */

/* Find the active job with the indicated pgid.  */
job *find_job (pid_t pgid){
  job *j;
  for (j = first_job; j; j = j->next){
    if (j->pgid == pgid){
      // printf("Found job by process ID! \n");
      return j;
    }
  }
  printf("Job doesn't exist!\n");
  return NULL;
}

job *find_job_by_id(int id){
  job *j;
  for (j = first_job; j; j = j->next){
    if (j->tag == id){
      // printf("Found job by ID ! %d\n",id);
      return j;
    }
  }
  printf("Job doesn't exist!\n");
  return NULL;
}

void launch_process (char* argv[],
                int infile, int outfile, int errfile,
                int mode){
  pid_t pid = getpid();

      /* Put the process into the process group and give the process group
         the terminal, if appropriate.
         This has to be done both by the shell and in the individual
         child processes because of potential race conditions.  */
      setpgid(pid,pid);
      if (mode==FOREGROUND){
        tcsetpgrp (shell_terminal, pid);
      }

      /* Set the handling for job control signals back to the default.  */

      struct sigaction sa = {0};
      struct sigaction child_sa = {0};

      sa.sa_handler = SIG_DFL;
      sa.sa_flags = 0;

      signal(SIGINT, SIG_IGN);
      sigaction(SIGINT, &sa, NULL);
      sigaction(SIGTSTP, &sa, NULL);
      sigaction(SIGQUIT, &sa, NULL);
      sigaction(SIGTTIN, &sa, NULL);
      sigaction(SIGTTOU, &sa, NULL);

      child_sa.sa_sigaction = child_signal_handler;
      child_sa.sa_flags = SA_SIGINFO;
      sigaction(SIGCHLD, &child_sa, NULL);


  /* Set the standard input/output channels of the new process.  */
  if (infile != STDIN_FILENO)
    {
      dup2 (infile, STDIN_FILENO);
      close (infile);
    }
  if (outfile != STDOUT_FILENO)
    {
      dup2 (outfile, STDOUT_FILENO);
      close (outfile);
    }
  if (errfile != STDERR_FILENO)
    {
      dup2 (errfile, STDERR_FILENO);
      close (errfile);
    }

  /* Exec the new process.  Make sure we exit.  */
  // printf("Entering execution \n");
  if(execvp(argv[0], argv) < 0) {
    printf("\t- %s\n", strerror(errno));
  }
  perror ("execvp");
  exit (127);
}
/* Put job j in the foreground.  If cont is nonzero,
   restore the saved terminal modes and send the process group a
   SIGCONT signal to wake it up before we block.  */

void put_job_in_foreground (job *j, int cont)
{
  /* Put the job into the foreground.  */
  if(j==NULL){
    // printf("INVALID JOB");
    return;
  }
  j->mode = FOREGROUND;
  tcsetpgrp (shell_terminal, j->pgid);
  if(j->runningStatus=PAUSE){
    j->runningStatus = RUNNING;
  }
  /* Send the job a continue signal, if necessary.  */
  if (cont)
    {
      // printf("Continue job\n");
      tcsetattr (shell_terminal, TCSADRAIN, &j->tmodes);
      if (kill (-j->pgid, SIGCONT) < 0)
        perror ("kill (SIGCONT)");
    }
    else{
      j->runningStatus = RUNNING;
    }

  /* Wait for it to report.  */
  wait_for_job (j);

  /* Put the shell back in the foreground.  */
  tcsetpgrp (shell_terminal, shell_pgid);

  /* Restore the shell’s terminal modes.  */
  tcgetattr (shell_terminal, &j->tmodes);
  tcsetattr (shell_terminal, TCSADRAIN, &shell_tmodes);
}

/* Put a job in the background.  If the cont argument is true, send
   the process group a SIGCONT signal to wake it up.  */

void put_job_in_background (job *j, int cont)
{
  if(j==NULL){
    // printf("INVALID JOB\n");
    return;
  }
  j->mode = BACKGROUND;
  /* Send the job a continue signal, if necessary.  */
  if (cont){
      if (kill (-j->pgid, SIGCONT) < 0){
      perror ("kill (SIGCONT)\n");
    }else{
        j->runningStatus = RUNNING;
      }
    }
}

/* Check for processes that have status information available,
   blocking until all processes in the given job have reported.  */

void wait_for_job (job *j)
{
  int terminationStatus;
  //waiting for the job to be done.
  while (waitpid(j->pgid, &terminationStatus, WNOHANG) == 0) {
    if (j->runningStatus == PAUSE)
      return;
  }
  shell_returncode = terminationStatus;
  icsh_cleanjob(j);
}


void init_shell ()
{
  /* See if we are running interactively.  */
  shell_terminal = STDIN_FILENO;
  shell_is_interactive = isatty (shell_terminal);

  if (shell_is_interactive)
    {
      /* Loop until we are in the foreground.  */
      while (tcgetpgrp (shell_terminal) != (shell_pgid = getpgrp ()))
        kill (- shell_pgid, SIGTTIN);

          struct sigaction sa = {0};
          struct sigaction sai = {0};
          struct sigaction child_sa = {0};

          sa.sa_handler = SIG_IGN;
          sa.sa_sigaction = signal_handler;
          sa.sa_flags = 0;

          sai.sa_handler = SIG_IGN;
          sai.sa_flags = 0;

          signal(SIGINT, SIG_IGN);
          sigaction(SIGINT, &sa, NULL);
          sigaction(SIGTSTP, &sa, NULL);
          sigaction(SIGQUIT, &sa, NULL);
          sigaction(SIGTTIN, &sai, NULL);
          sigaction(SIGTTOU, &sai, NULL);

          child_sa.sa_sigaction = child_signal_handler;
          child_sa.sa_flags = SA_SIGINFO;
          sigaction(SIGCHLD, &child_sa, NULL);


      /* Put ourselves in our own process group.  */
      shell_pgid = getpgrp(); //get grp id
      if (setpgid (shell_pgid, shell_pgid) < 0)
        {
          perror ("Couldn't put the shell in its own process group\n");
          exit (1);
        }

      /* Grab control of the terminal.  */
      tcsetpgrp (shell_terminal, shell_pgid);

      /* Save default terminal attributes for shell.  */
      tcgetattr (shell_terminal, &shell_tmodes);
    }
}

int icsh_cleanjob(job *j){
  if(j!=NULL){
    job* currentjob;
    job* previousjob;
    previousjob = first_job;
    currentjob = first_job->next;
  //if the given job is the fist job in list
    if(j->tag == previousjob->tag){
        previousjob = previousjob->next; //move the previousjob to the next one as we want to get rid of this
        first_job = currentjob; // change the head queue to be the next job;
      }
      else{
        while(currentjob!=NULL){
          if(j->tag == currentjob->tag){
            previousjob->next = currentjob->next;
          }
          previousjob = currentjob;
          currentjob = currentjob->next;
      }
  }
  //decrease the job count;
    // printf("Succesfully clean : %s\n", j->name);
    jobcount--;
    free(j->name);
    free(j);
    j = NULL;
    return 1;
  }
}

void icsh_execute(int mode, char** arg, int input, int error, int output, char* commandName){
  pid_t pid;
  pid = fork();
  if(pid==0){
    //child process
    launch_process(arg, input, output, error, mode);
  }
  else if(pid > 0){
    // parent process
    // do the job
    setpgid(pid, pid);
    job* newjob = malloc(sizeof(job));
    newjob->name = malloc(sizeof(commandName) * sizeof(char)); //name the job as the command
    strcpy(newjob->name, commandName);
    newjob->pgid = pid;
    tcgetattr(shell_terminal, &newjob->tmodes);
    newjob->stdin = input;
    newjob->stdout = output;
    newjob->stderr = error;
    newjob->completed = 0;
    newjob->stopped = 0;
    newjob->runningStatus = RUNNING;
    newjob->mode = mode;
    newjob->next = NULL;
    //If this is the first job
    if(first_job == NULL){
      jobcount++;
      newjob->tag = jobcount;
      first_job = newjob;
    } else{
      job* currentjob = first_job;
      //scanning to find the last job in list
      while(currentjob-> next !=NULL){
        currentjob = currentjob->next;
      }
      //the next job is now null; put our newest job there
      newjob->tag = currentjob->tag+1;
      currentjob->next = newjob;
      jobcount++;
    }
    if(mode == FOREGROUND){
      put_job_in_foreground(newjob,0);
    }else{
      put_job_in_background(newjob,0);
      tcsetpgrp(shell_terminal,shell_pgid);
    }
  }
  else{
    printf("Forking error\n");
    loopstatus = -1;
    exit(EXIT_FAILURE);
  }
}


void icsh_parseline(char* line, int* argcounter, char* argv[], int* mode, int* infile, int* outfile, int* errfile)
{
  char* line_buffer;
  line_buffer = strtok(line,TOK_DELIM);

  int file;
  *infile = STDIN_FILENO;
  *outfile = STDOUT_FILENO;
  *errfile = STDERR_FILENO;

  //tokenizing input with white space
  while (line_buffer != NULL && *argcounter < MAX_ARG) {
    argv[(*argcounter)++] = line_buffer;
    line_buffer = strtok(NULL, TOK_DELIM);
  }
  if (*argcounter > 2) {
    //put in background job
    if(strcmp(argv[*argcounter-1], "&") == 0){
      // printf("Putting jobs in background \n");
    *mode = BACKGROUND;
    argv[(*argcounter)-1] = NULL;
    (*argcounter)--;
    } else {
      argv[*argcounter] = NULL;
    }
  }
  if (*argcounter >= 3){
    if(strcmp(argv[(*argcounter)-2], "<") == 0) {
      file = open(argv[(*argcounter)-1], O_RDONLY);
      // printf("redirecting I/O: command < file \n");
    if (file > 0) //if it's actually a file
      *infile = file; //redirect it to input file
    argv[(*argcounter)-2] = NULL;//strip off the "<"
    (*argcounter) -= 2;
  } else if (strcmp(argv[(*argcounter)-2], ">") == 0) {
    file = open(argv[(*argcounter)-1], O_CREAT | O_WRONLY, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
    if (file > 0)//if it's actually a file
      // printf("aredirecting I/O: command > file \n");
      *outfile = file;//redirect it to output file
    argv[(*argcounter)-2] = NULL; //strip off the ">"
    (*argcounter) -= 2;
    }
  }
}

char* icsh_readline(int* lenline){
  char *inputline = NULL;
  ssize_t bufsize = 0; // getline will malloc the appropriate buffer size for us.
  *lenline = getline(&inputline, &bufsize, stdin);
  if (*lenline < 0) {
    if (errno == EINTR){
      clearerr(stdin);
      fflush(stdout);
    } else if (errno == 0) {
      loopstatus = -1;
    }
    printf("\n");
  }if(inputline!=NULL){
    inputline[(*lenline)-1] = '\0'; //get rid of newline
  }
  return inputline;
}

int sizeOfInit(void){
  return sizeof(builtin_str)/(sizeof(char*));
}

int checkBuiltint(char** args){
  int i;
  if (args[0] == NULL){
    return -1;
  }
  for (i = 0; i < sizeOfInit(); i++) {
    if (strcmp(args[0], builtin_str[i]) == 0) {
      return (*builtin_func[i])(args);
    }
  }
  return 0;
}

int icsh_bg(char** args){
  if(args[1] != NULL){
    int job_id;
    job* job;
    char* str;
    job_id = strtol(args[2], &str, BASETEN);
    if(args[1][0] != str[0] && str[0] == '\0'){
      job = find_job_by_id(job_id);
      if(job!=NULL){
        put_job_in_background(job,1);
        // printf("successfully put job in background\n");
        return 1;
      }
      else{
        perror("error! invalid id");
      }
    }
    else{
      perror("error! invalid id");
    }
  }
  return -1;
}

int icsh_help(char** args){
  printf("Succesfully call for help");
  printf("but no one comes \n");
  return 1;
}

int icsh_exit(char** args){
  loopstatus = -1;
  return 1;
}


int icsh_echo(char** args){
  if(args[1] == NULL){
    return -1;
  }
  else if(args[1][0] == '$' && args[1][1] == '?'){
    printf("Latest response code : %d\n", shell_returncode);
    return 1;
  }
  return 0;
}

int icsh_fg(char** args){
  if(args[1] != NULL){
    long job_id;
    job* job;
    char* str;
    // printf("------Entering background to foreground mode ------\n");
    job_id = strtol(args[2],&str,BASETEN);
    if(args[1][0] != str[0] && str[0] == '\0'){
      job = find_job_by_id(job_id);
      if(job!=NULL){
        printf("successfully put job in fg\n");
        put_job_in_foreground(job,1);
        return 1;
      }
      else{
        perror("error! invalid process id");
      }
    }
    else{
      perror("error! invalid id");
    }
  }
  return -1;
}

int icsh_jobs(char** args){
  job* currentjob = first_job;
  // do_job_notification();
  if(currentjob!=NULL){
  while(currentjob!=NULL){
    printf("[%d] : ", currentjob->tag);
    if(currentjob->runningStatus == PAUSE){
      printf("%s SUSPENDED \n", currentjob->name);
    }
    else if(currentjob->runningStatus == RUNNING){
      printf("%s RUNNING \n", currentjob->name);
    }
    else{
      printf("%s UNKNOWN \n", currentjob->name);
    }
    currentjob = currentjob->next;
  }
  printf("Succesfully print all jobs\n");
  }
  else{
    printf("There's no job to print \n");
  }
  return 1;
}

int main(){
      // init_shell();
      int mode; // foreground, background.
      int input,error,output;
      char *line;
      loopstatus = 1;
      int default_implement = -1;
      int lenline;
      char *argv[MAX_ARG];
      int argcounter; //argument counter
      char* fullcommand;
      //declare input error output
      printf("You're now using IC Shell by Gift version 1.409\n");
      do {
        init_shell();
        pid_t pid = getpid();
        printf("IC shell > ");
        line = icsh_readline(&lenline);
        argcounter = 0;
        mode = FOREGROUND;
        icsh_parseline(line, &argcounter, argv, &mode, &input, &output, &error);
        argv[argcounter] = NULL;
        //if the user put in argument
        if (argcounter > 0) {
            // printf("Processing command \n");
            default_implement = checkBuiltint(argv);
            if(default_implement == 0){
              fullcommand = (char*) malloc(lenline * sizeof(char));
              strncpy(fullcommand, line, lenline);
              icsh_execute(mode, argv, input, error, output, fullcommand);
          //check build in
            }
            else if(default_implement == -1){
              printf("Error: bad argument\n");
              fflush(stdout);
            }
          }
          // printf("------End one process------\n");
          fflush(stdout);
        free(line);
        if(default_implement == 0){
          free(fullcommand);
          fullcommand = NULL;
        }
        line = NULL;
        fflush(stdout);
  } while (loopstatus >= 0);
  return EXIT_SUCCESS;
}
