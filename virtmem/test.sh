#!/bin/bash

declare -a algorithm=("rand" "fifo" "lru")
declare -a function=("sort" "scan" "focus")
PAGE_NUM=100

for frame_num in $(seq 3 100);
do
  for alg in ${algorithm[@]};
  do
    for func in ${function[@]};
    do

    filename="result-$frame_num-$alg-$func"

    ./virtmem $PAGE_NUM $frame_num $alg $func > ./result/$filename.txt

    done
  done
done
