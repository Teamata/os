/*
Main program for the virtual memory project.
Make all of your modifications to this file.
You may add or rearrange any code or data as you need.
The header files page_table.h and disk.h explain
how to use the page table and disk interfaces.
*/

#include "page_table.h"
#include "disk.h"
#include "program.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

struct data {
    int page_faults;
    int disk_reads;
    int disk_writes;
    int evictions;
};
struct data data;

typedef struct frame_table{
	int page;
	int bits;
	int isEmpty;
} frame_table;

struct node_t{
  int data;
  struct node_t *next;
};
typedef struct node_t node;

typedef struct{
  node *head;
}linkedlist;

linkedlist ll;
const char *mode;
struct frame_table *fakeTable = NULL;
struct disk *disk = NULL;
char *virtmem;
char *physmem;
const char *program;
int freeframes;
int totalframes;
int getFreeFrame();
void print_stats();
void eviction(struct page_table *pt, int frame_index);
void rand_page_fault_handler(struct page_table *pt, int page);
void fifo_update(int i, linkedlist *ll);
int fifo_eviction(struct page_table *pt,linkedlist *ll);

void init_list(linkedlist *ll){
  ll->head = NULL;
  return;
}

void free_linkedlist(linkedlist *ll){
  node *current = ll->head;
  while(current != NULL){
    node *temp = current;
    current = current->next;
    free(temp);
  }
}

void fifo_update(int i, linkedlist *ll){
  node *current = ll->head;
  // printf("recently added index : %d\n", i);
  if(ll->head == NULL){
    node *temp = (node*)malloc(sizeof(node));
    temp->data = i;
    ll->head = temp;
  }
  else{
    while (current->next != NULL) {
        current = current->next;
    }
    current->next = malloc(sizeof(node));
    current->next->data = i;
    current->next->next = NULL;
  }
}

int fifo_eviction(struct page_table *pt, linkedlist *ll){
  node *temp = ll->head;
  int frame_index = temp->data;
  ll->head = ll->head->next;
  free(temp);
  data.evictions+=1;
	if((fakeTable[frame_index].bits & PROT_WRITE) == (PROT_WRITE)){
		data.disk_writes+=1;
		disk_write(disk, fakeTable[frame_index].page, &physmem[frame_index * PAGE_SIZE]);
	}
  fakeTable[frame_index].isEmpty = 0;
  fakeTable[frame_index].bits = PROT_NONE;
  page_table_set_entry(pt, fakeTable[frame_index].page ,frame_index,PROT_NONE);
  return frame_index;
}

int getFreeFrame(){
	for(int i = 0; i<totalframes;i++){
		if(fakeTable[i].isEmpty == 0){
			return i;
		}
	}
	return -1;
}


void print_stats() {
    printf("\n-----Statistics-----: \n");
		printf(" page faults : %d \n", data.page_faults);
		printf(" disk reads : %d \n" , data.disk_reads);
		printf(" disk writes : %d \n", data.disk_writes);
		printf(" evictions : %d \n" , data.evictions);
}

void eviction(struct page_table *pt, int frame_index){
	//if the bits is dirty
	data.evictions+=1;
	if((fakeTable[frame_index].bits & PROT_WRITE) == (PROT_WRITE)){
		data.disk_writes+=1;
		disk_write(disk, fakeTable[frame_index].page, &physmem[frame_index * PAGE_SIZE]);
	}
	fakeTable[frame_index].isEmpty = 0;
	fakeTable[frame_index].bits = PROT_NONE;
	page_table_set_entry(pt, fakeTable[frame_index].page ,frame_index,PROT_NONE);
}

void fifo_page_fault_handler(struct page_table *pt, int page){
  data.page_faults+=1;
	int frame,permissionbits;
	int frame_index;
	page_table_get_entry(pt, page, &frame, &permissionbits);
  // printf("entry = %d\n", frame);
	if(permissionbits == PROT_NONE){
		permissionbits |= PROT_READ;
    // printf("test\n");
    frame_index = getFreeFrame();
    // printf("before enter %d\n", frame_index);
		if(frame_index == -1){
			frame_index = fifo_eviction(pt, &ll);
      // printf("passed fifo eviction\n");
      // printf("new index : %d\n", frame_index);
		}
    // printf("after enter %d\n", frame_index);
      fifo_update(frame_index, &ll);
      data.disk_reads+=1;
			disk_read(disk, page, &physmem[frame_index * PAGE_SIZE]);
	} else {
			permissionbits |= PROT_WRITE;
			frame_index = frame;
	}
	page_table_set_entry(pt, page, frame_index, permissionbits);
	fakeTable[frame_index].bits = permissionbits;
	fakeTable[frame_index].isEmpty = 1;
	fakeTable[frame_index].page = page;
}

void rand_page_fault_handler(struct page_table *pt, int page){
  data.page_faults+=1;
	int frame,permissionbits;
	int frame_index;
	page_table_get_entry(pt, page, &frame, &permissionbits);
	if(permissionbits == PROT_NONE){
    frame_index = getFreeFrame();
		permissionbits |= PROT_READ;
		//should distinct which one we're check in here.
		if(frame_index == -1){
			//random number in the frame to replace
			frame_index = rand() % totalframes;
			eviction(pt,frame_index);
			//randomize and eviction.
		}
		  data.disk_reads+=1;
			disk_read(disk, page, &physmem[frame_index * PAGE_SIZE]);
	} else {
			permissionbits |= PROT_WRITE;
			frame_index = frame;
	}
	page_table_set_entry(pt, page, frame_index, permissionbits);
	fakeTable[frame_index].bits = permissionbits;
	fakeTable[frame_index].isEmpty = 1;
	fakeTable[frame_index].page = page;
}

void page_fault_handler( struct page_table *pt, int page )
{
	if(!strcmp(mode,"rand")){
    rand_page_fault_handler(pt, page);
  }
  else if(!strcmp(mode,"fifo")){
    fifo_page_fault_handler(pt,page);
  }
}

int main( int argc, char *argv[] )
{
  linkedlist ll;
	if(argc!=5) {
		printf("use: virtmem <npages> <nframes> <rand|fifo|lru> <sort|scan|focus>\n");
		return 1;
	}

	int npages = atoi(argv[1]);
	int nframes = atoi(argv[2]);
  mode = argv[3];
  if(!strcmp(mode,"fifo")){
    init_list(&ll);
  }
	program = argv[4];
  printf("%s\n", program);
	disk = disk_open("myvirtualdisk",npages);
	if(!disk) {
		fprintf(stderr,"couldn't create virtual disk: %s\n",strerror(errno));
		return 1;
	}


	struct page_table *pt = page_table_create( npages, nframes, page_fault_handler );
	if(!pt) {
		fprintf(stderr,"couldn't create page table: %s\n",strerror(errno));
		return 1;
	}

	virtmem = page_table_get_virtmem(pt);

	physmem = page_table_get_physmem(pt);

	freeframes = nframes;
	totalframes = nframes;
	fakeTable = malloc(nframes * sizeof(frame_table));
	memset(fakeTable,0,nframes * sizeof(frame_table));

	if(!strcmp(program,"sort")) {
		sort_program(virtmem,npages*PAGE_SIZE);

	} else if(!strcmp(program,"scan")) {
		scan_program(virtmem,npages*PAGE_SIZE);

	} else if(!strcmp(program,"focus")) {
		focus_program(virtmem,npages*PAGE_SIZE);

	} else {
		fprintf(stderr,"unknown program: %s\n",argv[4]);

	}
  if(!strcmp(mode,"fifo")){
    free_linkedlist(&ll);
  }
  free(fakeTable);
	page_table_print(pt);
	page_table_delete(pt);
	disk_close(disk);
	print_stats();

	return 0;
}
