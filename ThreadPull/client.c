/**
 * client.c, copyright 2001 Steve Gribble
 *
 * The client is a single-threaded program; it sits in a tight
 * loop, and in each iteration, it opens a TCP connection to
 * the server, sends a request, and reads back a response.
 */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <ctype.h>
#include <errno.h>
#include <sys/time.h>

#include "lib/socklib.h"
#include "common.h"

/**
 * This program should be invoked as "./client hostname portnumber",
 * for example, "./client spinlock 4342".
 */

int main(int argc, char **argv) {
  int  socket_talk, i;
  char request[REQUEST_SIZE];
  char response[RESPONSE_SIZE];
  // long requests = 1000000;
  //
  // struct timeval tv = {0};
  // gettimeofday(&tv, NULL);
  // long start;
  // long end, taskPerSec;
  // int taskCount = 0;
  // long t;


  if (argc != 3) {
    fprintf(stderr,
	    "(CLIENT): Invoke as  'client machine.name.address socknum'\n");
    exit(1);
  }

  // initialize request to some silly data
  for (i=0; i<REQUEST_SIZE; i++) {
    request[i] = (char) i%255;
  }

  // spin forever, opening connections, and pushing requests
  // gettimeofday(&tv, NULL);
  // start = tv.tv_sec;
  while(1) {
    int result;
    // open up a connection to the server
    if ((socket_talk = sconnect(argv[1], argv[2])) < 0) {
      perror("(CLIENT): sconnect");
      exit(1);
    }

    // write the request
    result = correct_write(socket_talk, request, REQUEST_SIZE);
    if (result == REQUEST_SIZE) {
      // read the response
      result = correct_read(socket_talk, response, RESPONSE_SIZE);
    }
    close(socket_talk);
    // requests--;
    // printf("%ld\n", requests);
  }
  // gettimeofday(&tv, NULL);
  // end = tv.tv_sec;
  // t = end-start;
  // taskPerSec = ((double)1000000/t);
  // printf("%ld tasks per seconds \n", taskPerSec);

  return 0;
}
