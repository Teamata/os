/**
* server.c, copyright 2001 Steve Gribble
*
* The server is a single-threaded program.  First, it opens
* up a "listening socket" so that clients can connect to
* it.  Then, it enters a tight loop; in each iteration, it
* accepts a new connection from the client, reads a request,
* computes for a while, sends a response, then closes the
* connection.
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <errno.h>
#include <sys/time.h>



#include "lib/socklib.h"
#include "common.h"
#include "threadpool.h"

extern int errno;

#define TOTALTASKS 100000

int   setup_listen(char *socketNumber);
char *read_request(int fd);
char *process_request(char *request, int *response_length);
void  send_response(int fd, char *response, int response_length);
struct CommandLine* parse_command(int argc, char** argv);
void connection(void* arg);

struct CommandLine* argument;
struct CommandLine {
    char *port;
    int thread_num;
    int loop_num;
};

volatile long int taskCount = 0;
long start = 0;
long current, end, taskPerSec;
long t;
long meet_time = 0;
struct timeval tv = {0};
threadpool threads;
/**
* This program should be invoked as "./server <socketnumber>", for
* example, "./server 4342".
*/

int main(int argc, char **argv)
{
    char buf[1000];
    int  socket_listen;
    int  socket_talk;
    int  dummy, len;

    gettimeofday(&tv, NULL);

    if (argc < 2 || argc > 6)
    {
        fprintf(stderr, "(SERVER): Invoke as  './server socknum'\n");
        fprintf(stderr, "(SERVER): for example, './server 4434'\n");
        fprintf(stderr, "(SERVER): for example, './server socknum -l 50 -t 50'\n");
        exit(-1);
    }

    argument = parse_command(argc,argv);

    threads = create_threadpool(argument->thread_num);

    /*
    * Set up the 'listening socket'.  This establishes a network
    * IP_address:port_number that other programs can connect with.
    */

    socket_listen = setup_listen(argument->port);

    /*
    * Here's the main loop of our program.  Inside the loop, the
    * one thread in the server performs the following steps:
    *
    *  1) Wait on the socket for a new connection to arrive.  This
    *     is done using the "accept" library call.  The return value
    *     of "accept" is a file descriptor for a new data socket associated
    *     with the new connection.  The 'listening socket' still exists,
    *     so more connections can be made to it later.
    *
    *  2) Read a request off of the listening socket.  Requests
    *     are, by definition, REQUEST_SIZE bytes long.
    *
    *  3) Process the request.
    *
    *  4) Write a response back to the client.
    *
    *  5) Close the data socket associated with the connection
    */
    // gettimeofday(&tv,NULL);
    // start = tv.tv_sec;
    while(1) {
        socket_talk = saccept(socket_listen);  // step 1
        if (socket_talk < 0) {
            fprintf(stderr, "An error occured in the server; a connection\n");
            fprintf(stderr, "failed because of ");
            perror("");
            exit(1);
        }
        dispatch(threads, connection, (void *) socket_talk);
        // taskCount++;

        // if(taskCount == 1){
        //   gettimeofday(&tv, NULL);
        //   start = tv.tv_sec;
        // }

        // if(taskCount > TOTALTASKS){
        //   printf("Done all tasks\n");
        //   gettimeofday(&tv,NULL);
        //   current = tv.tv_sec;
        //   t = (current-start);
        //   printf("total seconds taken %ld\n", t);
        //   taskPerSec = ((double)taskCount/t);
        //   printf("%ld\n",taskPerSec);
        //   break;
        // }
        if(meet_time){
          taskPerSec = ((double)taskCount/t);
          break;
        }
    }
    printf("loop num : %d\n", argument->loop_num);
    printf("threadnum :%d\n", argument->thread_num);
    printf("%ld tasks per seconds \n", taskPerSec);
}



/**
* This function accepts a string of the form "5654", and opens up
* a listening socket on the port associated with that string.  In
* case of error, this function simply bonks out.
*/

int setup_listen(char *socketNumber) {
    int socket_listen;

    if ((socket_listen = slisten(socketNumber)) < 0) {
        perror("(SERVER): slisten");
        exit(1);
    }

    return socket_listen;
}

/**
* This function reads a request off of the given socket.
* This function is thread-safe.
*/

char *read_request(int fd) {
    char *request = (char *) malloc(REQUEST_SIZE*sizeof(char));
    int   ret;

    if (request == NULL) {
        fprintf(stderr, "(SERVER): out of memory!\n");
        exit(-1);
    }

    ret = correct_read(fd, request, REQUEST_SIZE);
    if (ret != REQUEST_SIZE) {
        free(request);
        request = NULL;
    }
    return request;
}

struct CommandLine* parse_command(int argc, char **argv) {
    struct CommandLine* arg = (struct CommandLine *) malloc(sizeof (struct CommandLine));
    //default stats
    arg->loop_num = 1000;
    arg->port = argv[1];
    arg->thread_num = 2;
    int opt;
    while ((opt = getopt(argc, argv, "l:t:")) !=-1 ) {
        if(opt == 'l'){
              arg->loop_num = atoi(optarg);}
        if(opt == 't'){
              arg->thread_num = atoi(optarg);
            }
        }
    printf("loop_num: %d - port: %s - thread_num: %d\n", arg->loop_num, arg->port, arg->thread_num);
    return arg;
}

/**
* This function crunches on a request, returning a response.
* This is where all of the hard work happens.
* This function is thread-safe.
*/

// #define NUM_LOOPS 500000

char *process_request(char *request, int *response_length) {
    char *response = (char *) malloc(RESPONSE_SIZE*sizeof(char));
    int   i,j;

    // just do some mindless character munging here

    for (i=0; i<RESPONSE_SIZE; i++)
    response[i] = request[i%REQUEST_SIZE];

    for (j=0; j<argument->loop_num; j++) {
       // printf("loop number: %d\n", j);
        for (i=0; i<RESPONSE_SIZE; i++) {
            char swap;
            swap = response[((i+1)%RESPONSE_SIZE)];
            response[((i+1)%RESPONSE_SIZE)] = response[i];
            response[i] = swap;
        }
    }
    *response_length = RESPONSE_SIZE;
    return response;
}

void connection(void *arg){
      // printf("incoming connection \n");
      char *request = NULL;
      char *response = NULL;
      int socket_talk = (int) arg;
      if(start == 0){
        gettimeofday(&tv,NULL);
        start = tv.tv_sec;
      }
      request = read_request(socket_talk);  // step 2
        if (request != NULL) {
            int response_length;
            response = process_request(request, &response_length);  // step 3
            if (response != NULL) {
                send_response(socket_talk, response, response_length);  // step 4
            }
        }
    // printf("closing socket \n");
    close(socket_talk);  // step 5
    taskCount++;
    gettimeofday(&tv,NULL);
    current = tv.tv_sec;
    t = current-start;
    if(t >= 30){
      meet_time = 1;
    }
    // printf("%ld\n", taskCount);
        // clean up allocated memory, if any
    if (request != NULL){
        free(request);
      }
    if (response != NULL){
        free(response);
      }
}
