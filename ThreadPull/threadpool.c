/**
 * threadpool.c
 *
 * This file will contain your implementation of a threadpool.
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include "threadpool.h"

volatile int job_done = 0;

typedef struct job_st{
	void (*routine) (void*);
	void * arg;
	struct job_st* next;
} job;

typedef struct _threadpool_st {
   // you should fill in this structure with whatever you need
	int maxthreads;
	volatile int workingthread;
	volatile int jobsize;
	pthread_t *threads;
	job* head;
	job* tail;
	pthread_mutex_t mutex_pool;
	pthread_cond_t job_available;
	pthread_cond_t job_done;
	int shutdown;
	int no_more_job;
} _threadpool;

/* This function is the job function of the thread */
void* worker_thread(void* p) {
	_threadpool * pool = (_threadpool *) p;
	job* cur;
	while(1) {
		pthread_mutex_lock(&(pool->mutex_pool));

		if(pool->shutdown) {
      pthread_mutex_unlock(&(pool->mutex_pool));
      pthread_exit(NULL);
    }

		while(pool->jobsize == 0) {
      // pthread_mutex_unlock(&(pool->mutex_pool));
			pthread_cond_wait(&(pool->job_available),&(pool->mutex_pool));
		}

		// pool->workingthread += 1;
		// printf("workingthread : %d / %d \n", pool->workingthread, pool->maxthreads);

		cur = pool->head;

		pool->jobsize--;
		// pool->workingthread++;

		if(pool->jobsize == 0) {
			pool->head = NULL;
			pool->tail = NULL;
		}
		else {
			pool->head = cur->next;
		}
		pthread_mutex_unlock(&(pool->mutex_pool));

		(cur->routine) (cur->arg);
		free(cur);
		pthread_cond_signal(&(pool->job_done));

		pthread_mutex_lock(&(pool->mutex_pool));
		pool->workingthread -= 1;
		job_done += 1;
		// printf("> job done \n");
		// printf("> workingthread : %d / %d \n", pool->workingthread, pool->maxthreads);
		pthread_mutex_unlock(&(pool->mutex_pool));
		// if(pool->workingthread != 0){
		// 	pthread_cond_signal(&(pool->job_done));
		// }
	}
}

threadpool create_threadpool(int maxthreads_in_pool) {
  _threadpool *pool;
	int i;

  // sanity check the argument
  if ((maxthreads_in_pool <= 0) || (maxthreads_in_pool > MAXT_IN_POOL))
    return NULL;

  pool = (_threadpool *) malloc(sizeof(_threadpool));
  if (pool == NULL) {
    fprintf(stderr, "Out of memory creating a new threadpool!\n");
    return NULL;
  }

  pool->threads = (pthread_t*) malloc (sizeof(pthread_t) * maxthreads_in_pool);

  if(!pool->threads) {
    fprintf(stderr, "Out of memory creating a new threadpool!\n");
    return NULL;
  }

  pool->maxthreads = maxthreads_in_pool;
  pool->jobsize = 0;
  pool->head = NULL;
  pool->tail = NULL;
  pool->shutdown = 0;
  pool->no_more_job = 0;
	pool->workingthread = 0;

  if(pthread_mutex_init(&pool->mutex_pool,NULL)) {
    fprintf(stderr, "Mutex initiation error!\n");
	return NULL;
  }
  if(pthread_cond_init(&(pool->job_available),NULL)) {
    fprintf(stderr, "job_available initiation error!\n");
	return NULL;
  }
	if(pthread_cond_init(&(pool->job_done),NULL)) {
    fprintf(stderr, "job_done initiation error!\n");
	return NULL;
  }

  //make threads

  for (i = 0;i < maxthreads_in_pool;i++) {
	  if(pthread_create(&(pool->threads[i]),NULL,worker_thread,pool)) {
	    fprintf(stderr, "Thread initiation error!\n");
		return NULL;
	  }
  }
  return (threadpool) pool;
}


void dispatch(threadpool from_me, dispatch_fn dispatch_to_here,
	      void *arg) {
  _threadpool *pool = (_threadpool *) from_me;
	job * cur;
	int k;

	k = pool->jobsize;

	cur = (job*) malloc(sizeof(job));
	if(cur == NULL) {
		fprintf(stderr, "Out of memory creating a job struct!\n");
		return;
	}

	cur->routine = dispatch_to_here;
	cur->arg = arg;
	cur->next = NULL;

	pthread_mutex_lock(&(pool->mutex_pool));

	if(pool->no_more_job) {
		free(cur);
		return;
	}
	if(pool->jobsize == 0) {
		pool->head = cur;
		pool->tail = cur;
		pthread_cond_signal(&(pool->job_available));
	} else {
		pool->tail->next = cur;
		pool->tail = cur;
		pthread_cond_signal(&(pool->job_available));
	}
	pool->jobsize += 1;
	pool->workingthread += 1;
	// printf("> check from dispatch workingthread : %d / %d \n", pool->workingthread, pool->maxthreads);
	while(pool->workingthread>(pool->maxthreads)){
		// printf("> reaching maximum thread usage please wait!\n");
		pthread_cond_wait(&(pool->job_done),&(pool->mutex_pool));
	}
	pthread_mutex_unlock(&(pool->mutex_pool));
}

int num_job_done(){
	return job_done;
}

void destroy_threadpool(threadpool destroyme) {
	_threadpool *pool = (_threadpool *) destroyme;
	void* nothing;
	int i = 0;

	pthread_mutex_lock(&(pool->mutex_pool));
	pool->no_more_job = 1;

	while(pool->workingthread != 0) {
		pthread_cond_wait(&(pool->job_done),&(pool->mutex_pool));
	}
	pool->shutdown = 1;
	pthread_cond_broadcast(&(pool->job_available));
	pthread_mutex_unlock(&(pool->mutex_pool));

	for(;i < pool->maxthreads;i++) {
		pthread_join(pool->threads[i],&nothing);
	}

	free(pool->threads);

	pthread_mutex_destroy(&(pool->mutex_pool));
	pthread_cond_destroy(&(pool->job_available));
	pthread_cond_destroy(&(pool->job_done));
	return;
}
