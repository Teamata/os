/**
 * server.c, copyright 2001 Steve Gribble
 *
 * The server is a single-threaded program.  First, it opens
 * up a "listening socket" so that clients can connect to
 * it.  Then, it enters a tight loop; in each iteration, it
 * accepts a new connection from the client, reads a request,
 * computes for a while, sends a response, then closes the
 * connection.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <errno.h>
#include <sys/time.h>
#include "lib/socklib.h"
#include "common.h"
#include "threadpool.h"

#define MAX_NUM_LOOPS 500000
//#define

extern int errno;

int setup_listen(char *socketNumber);
char *read_request(int fd);
char *process_request(char *request, int *response_length);
void send_response(int fd, char *response, int response_length);
void conn_recv(void *arg);
void cmd_error();
void measure_time();
double time_diff(struct timeval *prior, struct timeval *latter);
struct CommandLineArgs* parse_command(char **args, int argc);

struct CommandLineArgs {
    char *port;
    int thread_num;
    int loop_num;
};

struct CommandLineArgs *args;
struct timeval *start;
struct timeval *current;
int count = 0;

int main(int argc, char **argv) {
    char buf[1000];
    int socket_listen;
    int socket_talk;
    int dummy, len;

    if (argc <= 2  || argc > 6) {
        cmd_error();
    }
    args = parse_command(argv, argc);

    threadpool tp = create_threadpool(args->thread_num);

    socket_listen = setup_listen(args->port);

    while (1) {
        socket_talk = saccept(socket_listen); // step 1

        if (socket_talk < 0) {
            fprintf(stderr, "An error occured in the server; a connection\n");
            fprintf(stderr, "failed because of ");
            perror("");
            exit(1);
        }

        measure_time();
        dispatch(tp, conn_recv, (void *) socket_talk);
    }
}

void measure_time() {

    const int SECONDS = 1;
    const int MILIS = SECONDS * 1000;

    if (start == NULL) {
        start = (struct timeval*) malloc(sizeof(struct timeval));
        gettimeofday(start, NULL);
        current = start;
        printf("Start Time: %ld.%06ld\n", start->tv_sec, start->tv_usec);
    } else {

        if (current == NULL){
            current = (struct timeval*)malloc(sizeof(struct timeval));
        }
        gettimeofday(current, NULL);


        if (time_diff(start, current) < (MILIS)) {
            count++;
            printf("count: %d time left: %f\n", count, time_diff(start, current));
        } else if (time_diff(start, current) >= (MILIS)) {
            printf("Measured for thread in pool %d - workload %d\n", args->thread_num, args->loop_num);
            printf("  throughput %.3f\n", (float) (count/time_diff(start, current)) / 1000);
            count = 0;
        }
    }
}

void cmd_error() {
    fprintf(stderr, "(SERVER): Invoke as  './server socknum -t [threadnum|2] -l [numloops|1000]'\n");
    fprintf(stderr, "(SERVER): for example, './server socknum -t 50'\n");
    fprintf(stderr, "(SERVER): for example, './server socknum -l 50 -t 50'\n");
    exit(-1);
}

double time_diff(struct timeval *prior, struct timeval *latter) {
    return (double) (latter->tv_usec - prior->tv_usec) / 1000.0L +
            (double) (latter->tv_sec - prior->tv_sec) * 1000.0L;
}

struct CommandLineArgs* parse_command(char **argv, int argc) {
    struct CommandLineArgs* arg = (struct CommandLineArgs *) malloc(sizeof (struct CommandLineArgs));
    arg->loop_num = 1000;
    arg->port = argv[1];
    arg->thread_num = 2;

    int opt;
    while ((opt = getopt(argc, argv, "l:t:")) != -1) {
        switch (opt) {
            case 'l':
                arg->loop_num = atoi(optarg);
                break;
            case 't':
                arg->thread_num = atoi(optarg);
                break;
        }
    }

    printf("loop_num: %d - port: %s - thread_num: %d\n", arg->loop_num, arg->port, arg->thread_num);
    return arg;
}

/**
 * This function accepts a string of the form "5654", and opens up
 * a listening socket on the port associated with that string.  In
 * case of error, this function simply bonks out.
 */

int setup_listen(char *socketNumber) {
    int socket_listen;

    if ((socket_listen = slisten(socketNumber)) < 0) {
        perror("(SERVER): slisten");
        exit(1);
    }

    return socket_listen;
}

/**
 * This function reads a request off of the given socket.
 * This function is thread-safe.
 */

char *read_request(int fd) {
    char *request = (char *) malloc(REQUEST_SIZE * sizeof (char));
    int ret;

    if (request == NULL) {
        fprintf(stderr, "(SERVER): out of memory!\n");
        exit(-1);
    }

    ret = correct_read(fd, request, REQUEST_SIZE);
    if (ret != REQUEST_SIZE) {
        free(request);
        request = NULL;
    }
    return request;
}

/**
 * This function crunches on a request, returning a response.
 * This is where all of the hard work happens.
 * This function is thread-safe.
 */

char *process_request(char *request, int *response_length) {
    char *response = (char *) malloc(RESPONSE_SIZE * sizeof (char));
    int i, j;

    // just do some mindless character munging here

    for (i = 0; i < RESPONSE_SIZE; i++)
        response[i] = request[i % REQUEST_SIZE];

    for (j = 0; j < args->loop_num; j++) {
        for (i = 0; i < RESPONSE_SIZE; i++) {
            char swap;

            swap = response[((i + 1) % RESPONSE_SIZE)];
            response[((i + 1) % RESPONSE_SIZE)] = response[i];
            response[i] = swap;
        }
    }
    *response_length = RESPONSE_SIZE;
    return response;
}

void conn_recv(void *arg) {
    int socket_talk = (int) arg;

    char *request = NULL;
    char *response = NULL;

    request = read_request(socket_talk); // step 2
    if (request != NULL) {
        int response_length;

        response = process_request(request, &response_length); // step 3
        if (response != NULL) {
            send_response(socket_talk, response, response_length); // step 4
        }
    }

    close(socket_talk);
    if (request != NULL)
        free(request);
    if (response != NULL)
        free(response);
}
