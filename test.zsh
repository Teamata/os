#!/usr/bin/zsh
algos=('rand' 'fifo' 'sech');
progs=('sort' 'scan' 'focus');

for ((i=3; i<101;i++)); do
    for a in $algos; do
	    for p in $progs; do
            echo $(./virtmem 100 $i $a $p | tail -3 | awk '{print $NF}') $i >> results1.out
	    done
    done;
    echo $i;
done;

